package com.hamon.walltmartchallenge.core.utils

abstract class UseCase<in T : Any?, out R: Any?> {
    abstract suspend operator fun invoke(params: T? = null): R?

}