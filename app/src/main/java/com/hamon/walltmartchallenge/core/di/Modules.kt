package com.hamon.walltmartchallenge.core.di

import com.google.android.libraries.places.api.Places
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.hamon.walltmartchallenge.data.datasource.ProductRemoteDatasource
import com.hamon.walltmartchallenge.data.datasource.ProductRemoteDatasourceImpl
import com.hamon.walltmartchallenge.data.repository.ProductsRepositoryImpl
import com.hamon.walltmartchallenge.domain.repository.ProductsRepository
import com.hamon.walltmartchallenge.domain.usecase.AddProductUseCase
import com.hamon.walltmartchallenge.domain.usecase.CreateUUIDUseCase
import com.hamon.walltmartchallenge.domain.usecase.DeleteProductUseCase
import com.hamon.walltmartchallenge.domain.usecase.GetProductListUseCase
import com.hamon.walltmartchallenge.domain.usecase.UpdateProductUseCase
import com.hamon.walltmartchallenge.presentation.viewmodel.ProductAddEditViewModel
import com.hamon.walltmartchallenge.presentation.viewmodel.ProductListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val googleServices = module {
    single { Places.createClient(get()) }
}

val firebaseModule = module {
    single { Firebase.database }
    single { Firebase.storage }
}

val remoteDatasource = module {
    single<ProductRemoteDatasource> { ProductRemoteDatasourceImpl(get()) }
}

val repository = module {
    single<ProductsRepository> { ProductsRepositoryImpl(get()) }
}

val useCase = module {
    single { AddProductUseCase(get()) }
    single { GetProductListUseCase(get()) }
    single { CreateUUIDUseCase(get()) }
    single { UpdateProductUseCase(get()) }
    single { DeleteProductUseCase(get()) }
}

val viewModel = module {
    viewModel { ProductListViewModel(get(), get()) }
    viewModel { ProductAddEditViewModel(get(), get(), get()) }
}