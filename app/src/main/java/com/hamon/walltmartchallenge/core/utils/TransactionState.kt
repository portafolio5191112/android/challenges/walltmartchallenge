package com.hamon.walltmartchallenge.core.utils

sealed class TransactionState<out T: Any>{
    data class Success<T: Any>(val value: T? = null) : TransactionState<T>()
    data class Error(val message: String = ""): TransactionState<Nothing>()
}
