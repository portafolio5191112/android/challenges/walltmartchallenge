package com.hamon.walltmartchallenge.core.utils

sealed class UiState<out T> {
    object Loading : UiState<Nothing>()
    data class Success<T>(val data: T? = null) : UiState<T>()
    data class Error(val message: String = "") : UiState<Nothing>()
}
