package com.hamon.walltmartchallenge.core.utils

sealed class ServiceTransactionState<out T: Any> {
    data class Success<T: Any>(val data: T? = null): ServiceTransactionState<T>()
    data class Error(val message: String): ServiceTransactionState<Nothing>()
}