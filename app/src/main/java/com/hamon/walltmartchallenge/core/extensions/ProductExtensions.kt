package com.hamon.walltmartchallenge.core.extensions

import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.hamon.walltmartchallenge.core.utils.ServiceTransactionState
import com.hamon.walltmartchallenge.data.model.LocationData
import com.hamon.walltmartchallenge.data.model.ProductData
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

fun ProductDomain.toProductData(): ProductData =
    ProductData(
        name = name,
        description = description,
        price = price,
        photos = photos,
        sku = id,
        quantity = quantity,
        location = LocationData(latitude = location.latitude, longitude = location.longitude)
    )

fun ProductData.toProductDomain(): ProductDomain =
    ProductDomain(
        name = name ?: "",
        description = description ?: "",
        price = price ?: 0.0,
        id = sku,
        photos = photos ?: mutableListOf(),
        quantity = quantity ?: 0,
        location = LatLng(location?.latitude ?: 0.0, location?.longitude ?: 0.0)
    )

suspend inline fun <reified T : Any> DatabaseReference.addValueEventListenerSingleData(): ServiceTransactionState<Any> =
    suspendCoroutine { continuation ->
        addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                continuation.resume(ServiceTransactionState.Success(data = snapshot.getValue(T::class.java)))
            }

            override fun onCancelled(error: DatabaseError) {
                continuation.resume(ServiceTransactionState.Error(message = error.message))
            }

        })
    }

suspend inline fun <reified T : Any> DatabaseReference.addValueEventListenerCollectionData(): ServiceTransactionState<Any> =
    suspendCoroutine { continuation ->
        addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                val listData = mutableListOf<T?>()

                snapshot.children.forEach {
                    listData.add(it.getValue(T::class.java))
                }
                continuation.resume(ServiceTransactionState.Success(data = listData))
            }

            override fun onCancelled(error: DatabaseError) {
                continuation.resume(ServiceTransactionState.Error(message = error.message))
            }

        })
    }

suspend inline fun DatabaseReference.setValueWithListeners(dataInput: Any): ServiceTransactionState<Any> =
    suspendCoroutine { continuation ->
        setValue(dataInput).addOnCompleteListener {
            continuation.resume(ServiceTransactionState.Success())
        }.addOnCanceledListener {
            continuation.resume(ServiceTransactionState.Error(message = "Verifica la conexión e intenta mas tarde"))
        }.addOnFailureListener {
            continuation.resume(ServiceTransactionState.Error(message = "Error desconocido"))
        }
    }

suspend inline fun DatabaseReference.deleteWithListeners(path: String): ServiceTransactionState<Any> =
    suspendCoroutine { continuation ->
        child(path).removeValue().addOnCompleteListener {
            continuation.resume(ServiceTransactionState.Success())
        }.addOnCanceledListener {
            continuation.resume(ServiceTransactionState.Error(message = "Verifica la conexión e intenta mas tarde"))
        }.addOnFailureListener {
            continuation.resume(ServiceTransactionState.Error(message = "Error desconocido"))
        }
    }

fun <T> T.toJson() : String {
    return Gson().toJson(this)
}

inline fun <reified T> String.fromJson(): T {
    return Gson().fromJson(this, T::class.java)
}