package com.hamon.walltmartchallenge.core.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<T>: ViewModel() {

    protected var isShowLoading: Boolean = false

    protected val _uiState: MutableLiveData<T> = MutableLiveData()
    val uiState : LiveData<T> get() = _uiState

}