package com.hamon.walltmartchallenge.core.utils

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar
import com.hamon.walltmartchallenge.R
import com.hamon.walltmartchallenge.presentation.screen.LoadingDialogFragment

abstract class BaseFragment : Fragment() {

    abstract val TAG: String

    private val loading = LoadingDialogFragment()

    fun showLoadingDialog( tag: String) {
        loading.show(childFragmentManager, tag)
    }

    fun hideLoadingDialog() {
        loading.dismiss()
    }

    fun showAlertDialog(title: String, message: String? = null, onContinueTap: (() -> Unit)? = null ) {
        AlertDialog.Builder(requireContext()).apply {
            setTitle(title)
            message?.let {
                setMessage(it)
            }
            setPositiveButton(
                R.string.remove_continue,
                DialogInterface.OnClickListener { dialog, _ ->
                    onContinueTap?.invoke()
                    dialog.dismiss()
                })
            setNegativeButton(
                R.string.remove_cancel,
                DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                })
        }.create().show()
    }

}