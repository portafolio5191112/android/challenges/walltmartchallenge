package com.hamon.walltmartchallenge.data.model

data class LocationData(
    val latitude: Double? = null,
    val longitude: Double? = null
)