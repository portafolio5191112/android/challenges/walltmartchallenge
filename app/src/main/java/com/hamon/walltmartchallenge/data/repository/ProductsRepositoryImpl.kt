package com.hamon.walltmartchallenge.data.repository

import com.hamon.walltmartchallenge.core.extensions.toProductData
import com.hamon.walltmartchallenge.core.extensions.toProductDomain
import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.data.datasource.ProductRemoteDatasource
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.domain.repository.ProductsRepository

class ProductsRepositoryImpl(private val datasource: ProductRemoteDatasource) : ProductsRepository {
    override suspend fun getProductList(): MutableList<ProductDomain> {
        return datasource.getProductList().map { it.toProductDomain() }.toMutableList()
    }

    override suspend fun addProduct(productDomain: ProductDomain): TransactionState<Any> {
        return datasource.addProduct(productDomain.toProductData())
    }

    override suspend fun updateProduct(productToUpdate: ProductDomain): TransactionState<Any> {
        return datasource.updateProduct(productToUpdate.toProductData())
    }

    override suspend fun deleteProduct(productId: String?): TransactionState<Any> {
        productId?.let {
            return datasource.deleteProduct(it)
        } ?: run {
            return TransactionState.Error(message = "El productId o Sku no debe ser vacio")
        }
    }

    override fun createUUID() = datasource.createUUID()

}