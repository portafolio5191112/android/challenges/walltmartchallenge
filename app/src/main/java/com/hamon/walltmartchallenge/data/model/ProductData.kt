package com.hamon.walltmartchallenge.data.model

import com.google.android.gms.maps.model.LatLng

data class ProductData(
    val name: String? = null,
    val description: String? = null,
    val price: Double? = null,
    val photos: MutableList<String>? = null,
    val sku: String? = null,
    val quantity: Int? = null,
    val location: LocationData? = null
)