package com.hamon.walltmartchallenge.data.datasource

import com.google.firebase.database.FirebaseDatabase
import com.hamon.walltmartchallenge.core.api.products.ProductCollections
import com.hamon.walltmartchallenge.core.extensions.addValueEventListenerCollectionData
import com.hamon.walltmartchallenge.core.extensions.deleteWithListeners
import com.hamon.walltmartchallenge.core.extensions.setValueWithListeners
import com.hamon.walltmartchallenge.core.utils.ServiceTransactionState
import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.data.model.ProductData
import java.util.UUID

class ProductRemoteDatasourceImpl(private val database: FirebaseDatabase) :
    ProductRemoteDatasource {

    private val databaseProducts = database.getReference(ProductCollections.PRODUCTS)
    override suspend fun getProductList(): MutableList<ProductData> {
        return when (val result =
            databaseProducts.addValueEventListenerCollectionData<ProductData>()) {
            is ServiceTransactionState.Success<*> -> result.data as MutableList<ProductData>
            is ServiceTransactionState.Error -> mutableListOf()
        }

    }

    override suspend fun addProduct(productData: ProductData): TransactionState<Any> {
        return when (databaseProducts.child(productData.sku ?: "")
            .setValueWithListeners(productData)) {
            is ServiceTransactionState.Success -> TransactionState.Success()
            is ServiceTransactionState.Error -> TransactionState.Error()
        }
    }

    override suspend fun updateProduct(productToUpdate: ProductData): TransactionState<Any> {
        return when (databaseProducts.child(productToUpdate.sku ?: "")
            .setValueWithListeners(productToUpdate)) {
            is ServiceTransactionState.Success -> TransactionState.Success()
            is ServiceTransactionState.Error -> TransactionState.Error()
        }
    }

    override suspend fun deleteProduct(productId: String): TransactionState<Any> {
        return when (databaseProducts.deleteWithListeners(productId)) {
            is ServiceTransactionState.Success -> TransactionState.Success()
            is ServiceTransactionState.Error -> TransactionState.Error()
        }
    }

    override fun createUUID(): String {
        return databaseProducts.push().key ?: UUID.randomUUID().toString()
    }

}

interface ProductRemoteDatasource {
    suspend fun getProductList(): MutableList<ProductData>

    suspend fun addProduct(productData: ProductData): TransactionState<Any>

    suspend fun updateProduct(productToUpdate: ProductData): TransactionState<Any>

    suspend fun deleteProduct(productId: String): TransactionState<Any>

    fun createUUID(): String
}