package com.hamon.walltmartchallenge.presentation.screen.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.hamon.walltmartchallenge.R
import com.hamon.walltmartchallenge.core.extensions.fromJson
import com.hamon.walltmartchallenge.core.utils.BaseFragment
import com.hamon.walltmartchallenge.databinding.FragmentProductDetailBinding
import com.hamon.walltmartchallenge.domain.entities.ProductDomain

class ProductDetailFragment : BaseFragment(), OnMapReadyCallback {

    private val binding: FragmentProductDetailBinding by lazy {
        FragmentProductDetailBinding.inflate(layoutInflater)
    }
    private var googleMap: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null
    private var productDetail: ProductDomain? = null

    override val TAG: String = this.javaClass.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            it.getString(PRODUCT_DETAIL)?.fromJson<ProductDomain>()?.let { product ->
                setupInfoProduct(product)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMap()
        setupToolbar()
    }

    private fun setupMap() {
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment?.getMapAsync(this)
    }


    private fun setupToolbar() {
        binding.topAppBar.apply {
            setNavigationOnClickListener {
                findNavController().popBackStack()
            }
        }
    }

    private fun setupInfoProduct(product: ProductDomain) {
        productDetail = product
        binding.apply {
            tvLabelProductName.text = product.name
            tvLabelProductDescription.text = product.description
            tvLabelProductPrice.text = "$${product.price}"
            tvLabelProductQuantity.text = product.quantity.toString()
            tvLabelProductSku.text = product.id
        }
    }

    private fun addNewMarker(location: LatLng) {
        googleMap?.let {
            it.addMarker(
                MarkerOptions()
                    .position(location)
            )
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15f))
        }
    }

    companion object {
        const val PRODUCT_DETAIL = "product_detail"
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        googleMap?.let {
            it.uiSettings.apply {
                setAllGesturesEnabled(false)
                isZoomControlsEnabled = false
                isMyLocationButtonEnabled = false
            }
        }
        productDetail?.let {
            addNewMarker(it.location)
        }
    }


}