package com.hamon.walltmartchallenge.presentation.viewmodel

import androidx.lifecycle.viewModelScope
import com.hamon.walltmartchallenge.core.utils.BaseViewModel
import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.core.utils.UiState
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.domain.usecase.DeleteProductUseCase
import com.hamon.walltmartchallenge.domain.usecase.GetProductListUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductListViewModel(
    private val getProductListUseCase: GetProductListUseCase,
    private val deleteProductUseCase: DeleteProductUseCase
) : BaseViewModel<UiState<MutableList<ProductDomain>>>() {

    fun getProductList() {
        showLoading()
        isShowLoading = true
        viewModelScope.launch(Dispatchers.IO) {
            //val asyncFunc = async { getProductListUseCase.invoke() }
            //val result = asyncFunc.await()
            handleState(getProductListUseCase.invoke())
        }
    }

    private fun handleState(state: TransactionState<MutableList<ProductDomain>>) {
        when (state) {
            is TransactionState.Success -> _uiState.postValue(
                UiState.Success(
                    data = state.value ?: mutableListOf<ProductDomain>()
                )
            )
            is TransactionState.Error -> {
                isShowLoading = true
                _uiState.postValue(UiState.Error())
            }
        }
    }

    fun deleteProduct(productId: String?) {
        showLoading()
        isShowLoading = true
        viewModelScope.launch {
            handleDeleteResult(deleteProductUseCase(productId))
        }
    }

    private fun showLoading() {
        if (isShowLoading.not()) {
            _uiState.value = UiState.Loading
        }
    }

    private fun handleDeleteResult(state: TransactionState<Any>) {
        when (state) {
            is TransactionState.Success -> getProductList()
            is TransactionState.Error -> {
                isShowLoading = false
                _uiState.postValue(UiState.Error())
            }
        }
    }

}