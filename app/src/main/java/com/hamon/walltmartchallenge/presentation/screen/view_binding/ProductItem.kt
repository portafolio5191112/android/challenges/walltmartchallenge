package com.hamon.walltmartchallenge.presentation.screen.view_binding

import android.view.View
import coil.load
import com.hamon.walltmartchallenge.R
import com.hamon.walltmartchallenge.databinding.ItemProductBinding
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.xwray.groupie.viewbinding.BindableItem

class ProductItem(
    private val productDomain: ProductDomain,
    private val onTapProduct: (ProductDomain) -> Unit,
    private val editTap: (ProductDomain) -> Unit,
    private val removeTap: (ProductDomain) -> Unit
) : BindableItem<ItemProductBinding>() {
    override fun bind(viewBinding: ItemProductBinding, position: Int) {
        viewBinding.apply {
            tvProductName.text = productDomain.name
            tvProductPrice.text = "Precio: $${productDomain.price}"
            tvProductQuantity.text = "Cantidad: ${productDomain.quantity}"
            ivRemove.setOnClickListener {
                removeTap.invoke(productDomain)
            }
            ivEdit.setOnClickListener {
                editTap.invoke(productDomain)
            }
            ivProductImage.load(productDomain.photos.first())
            containerProduct.setOnClickListener {
                onTapProduct.invoke(productDomain)
            }
        }
    }

    override fun getLayout() = R.layout.item_product

    override fun initializeViewBinding(view: View) = ItemProductBinding.bind(view)
}