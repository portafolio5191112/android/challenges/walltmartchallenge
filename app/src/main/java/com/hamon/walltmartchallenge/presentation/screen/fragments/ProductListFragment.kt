package com.hamon.walltmartchallenge.presentation.screen.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.hamon.walltmartchallenge.R
import com.hamon.walltmartchallenge.core.extensions.toJson
import com.hamon.walltmartchallenge.core.utils.BaseFragment
import com.hamon.walltmartchallenge.core.utils.UiState
import com.hamon.walltmartchallenge.databinding.FragmentProductListBinding
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.presentation.screen.fragments.ProductAddEditFragment.Companion.IS_BY_EDIT
import com.hamon.walltmartchallenge.presentation.screen.fragments.ProductAddEditFragment.Companion.PRODUCT_TO_EDIT
import com.hamon.walltmartchallenge.presentation.screen.fragments.ProductDetailFragment.Companion.PRODUCT_DETAIL
import com.hamon.walltmartchallenge.presentation.screen.view_binding.ProductItem
import com.hamon.walltmartchallenge.presentation.viewmodel.ProductListViewModel
import com.xwray.groupie.GroupieAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductListFragment : BaseFragment() {

    private val binding: FragmentProductListBinding by lazy {
        FragmentProductListBinding.inflate(layoutInflater)
    }

    private val viewModel: ProductListViewModel by viewModel()

    private val adapter: GroupieAdapter by lazy {
        GroupieAdapter()
    }

    override val TAG: String = this.javaClass.name

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupOnClickListener()
        setupObservable()
        setupRecycler()
        viewModel.getProductList()
    }

    private fun setupToolbar() {
        binding.topAppBar.title = "Productos"
    }

    private fun setupOnClickListener() {
        binding.btnAddProduct.setOnClickListener {
            findNavController().navigate(R.id.fragment_product_add_edit)
        }
    }

    private fun setupRecycler() {
        binding.rProduct.adapter = adapter
    }

    private fun setupObservable() {
        viewModel.uiState.observe(viewLifecycleOwner) {
            handleState(it)
        }
    }

    private fun handleState(state: UiState<MutableList<ProductDomain>>) {
        when (state) {
            is UiState.Loading -> showLoadingDialog(TAG)
            is UiState.Success -> handleSuccess(state)
            is UiState.Error -> handleError()
        }
    }

    private fun handleSuccess(successState: UiState.Success<MutableList<ProductDomain>>) {
        successState.data?.map {
            ProductItem(
                productDomain = it,
                removeTap = ::handleRemove,
                editTap = ::handleEdit,
                onTapProduct = ::onTapProduct
            )
        }?.let {
            adapter.clear()
            adapter.addAll(it)
        }
        hideLoadingDialog()
    }

    private fun onTapProduct(product: ProductDomain) {
        findNavController().navigate(
            R.id.fragment_product_detail,
            bundleOf(PRODUCT_DETAIL to product.toJson())
        )
    }

    private fun handleEdit(productDomain: ProductDomain) {
        findNavController().navigate(
            R.id.fragment_product_add_edit,
            bundleOf(IS_BY_EDIT to true, PRODUCT_TO_EDIT to productDomain.toJson())
        )
    }

    private fun handleRemove(productDomain: ProductDomain) {
        showAlertDialog(title = "Estas seguro que deseas borrar este elemento") {
            viewModel.deleteProduct(productDomain.id)
        }
    }

    private fun handleError() {
        hideLoadingDialog()
        showAlertDialog(
            title = "Hubo un error con la transaccion",
            message = "Se presento un problema con la transacción, intentelo mas tarde."
        )
    }
}