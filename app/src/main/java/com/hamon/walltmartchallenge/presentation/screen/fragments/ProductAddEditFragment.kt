package com.hamon.walltmartchallenge.presentation.screen.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.hamon.walltmartchallenge.R
import com.hamon.walltmartchallenge.core.extensions.fromJson
import com.hamon.walltmartchallenge.core.utils.BaseFragment
import com.hamon.walltmartchallenge.core.utils.PermissionUtils.isPermissionGranted
import com.hamon.walltmartchallenge.core.utils.UiState
import com.hamon.walltmartchallenge.databinding.FragmentProductAddEditBinding
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.presentation.viewmodel.ProductAddEditViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductAddEditFragment : BaseFragment(), OnMapReadyCallback,
    ActivityCompat.OnRequestPermissionsResultCallback {

    private val binding: FragmentProductAddEditBinding by lazy {
        FragmentProductAddEditBinding.inflate(layoutInflater)
    }

    private val viewModel: ProductAddEditViewModel by viewModel()
    private var autoCompleteFragmentPlaces: AutocompleteSupportFragment? = null
    private var mapFragment: SupportMapFragment? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var googleMap: GoogleMap? = null

    private var isByEdit: Boolean = false
    private var productToEdit: ProductDomain? = null
    override val TAG: String = this.javaClass.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isByEdit = it.getBoolean(IS_BY_EDIT)
            productToEdit = it.getString(PRODUCT_TO_EDIT)?.fromJson<ProductDomain>()
            viewModel.editProductModel(productToEdit)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupMap()
        setupOnChangeListener()
        setupObservable()
        showAddOrEditProduct()
        setupOnClickListener()
        setupAutocompletePlaces()
    }

    @SuppressLint("MissingPermission")
    private fun setupFusedLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        fusedLocationClient?.lastLocation?.addOnSuccessListener {
            addNewMarker(LatLng(it.latitude, it.longitude))
        }
    }

    private fun setupAutocompletePlaces() {
        autoCompleteFragmentPlaces =
            childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autoCompleteFragmentPlaces?.let { it ->
            it.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG))
            it.setOnPlaceSelectedListener(object : PlaceSelectionListener {
                override fun onError(status: Status) {
                    Log.e(TAG, "An error occurred: $status")
                }

                override fun onPlaceSelected(place: Place) {
                    place.latLng?.let { location ->
                        addNewMarker(location)
                    }
                }

            })
        }
        binding.containerPlaceSearch.isVisible = isByEdit.not()
    }

    private fun setupMap() {
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    private fun setupToolbar() {
        binding.topAppBar.apply {
            title =
                if (isByEdit.not()) getString(R.string.add_product_title) else getString(R.string.edit_product)
            setNavigationOnClickListener {
                findNavController().popBackStack()
            }
        }
    }

    private fun showAddOrEditProduct() {
        if (isByEdit.not()) {
            setupInfoToAddProduct()
        } else {
            setupInfoToEdit()
        }
    }

    private fun setupInfoToAddProduct() {
        if (isByEdit.not()) {
            viewModel.createSku()
            binding.btnAddProduct.text = getString(R.string.add_product)
        }
    }

    private fun setupInfoToEdit() {
        if (isByEdit) {
            productToEdit?.let {
                binding.apply {
                    binding.btnAddProduct.text = getString(R.string.update_product)
                    tietProductName.setText(it.name)
                    tietProductDescription.setText(it.description)
                    tietProductPrice.setText(it.price.toString())
                    tietProductQuantity.setText(it.quantity.toString())
                    tietProductSku.apply {
                        setText(it.id)
                        isEnabled = false
                    }
                }
            }
        }
    }

    private fun setupOnClickListener() {
        binding.apply {
            btnAddProduct.setOnClickListener {
                addOrEditProduct()
            }
        }
    }

    private fun addOrEditProduct() {
        if (isByEdit.not()) {
            addProduct()
        } else {
            editProduct()
        }
    }

    private fun addProduct() {
        viewModel.addProduct(
            name = binding.tietProductName.text.toString(),
            description = binding.tietProductDescription.text.toString(),
            price = binding.tietProductPrice.text.toString(),
            quantity = binding.tietProductQuantity.text.toString(),
            sku = binding.tietProductSku.text.toString()
        )
    }

    private fun editProduct() {
        viewModel.editProduct(
            name = binding.tietProductName.text.toString(),
            description = binding.tietProductDescription.text.toString(),
            price = binding.tietProductPrice.text.toString(),
            quantity = binding.tietProductQuantity.text.toString(),
        )
    }

    private fun setupObservable() {
        viewModel.apply {
            createUUID.observe(viewLifecycleOwner) {
                binding.tietProductSku.setText(it)
            }
            uiState.observe(viewLifecycleOwner) {
                handleState(it)
            }
            enableButtonContinue.observe(viewLifecycleOwner) {
                binding.btnAddProduct.isEnabled = it
            }
        }

    }

    private fun handleState(state: UiState<Any>) {
        when (state) {
            is UiState.Loading -> showLoadingDialog(TAG)
            is UiState.Success -> handleSuccess()
            is UiState.Error -> handleError()
        }
    }

    private fun handleSuccess() {
        hideLoadingDialog()
        findNavController().popBackStack()
    }

    private fun handleError() {
        hideLoadingDialog()
        showAlertDialog(
            title = "Hubo un error con la transaccion",
            message = "Se presento un problema con la transacción, intentelo mas tarde."
        ) {

        }
    }

    private fun setupOnChangeListener() {
        binding.apply {
            tietProductName.doOnTextChanged { text, _, _, _ ->
                viewModel.productName(text.toString())
                text?.toString()?.let {
                    checkLengthName(it.length)
                }
            }
            tietProductPrice.doOnTextChanged { text, _, _, _ ->
                viewModel.productPrice(text.toString())
                checkPriceEmptyField(text.toString())
            }
            tietProductSku.doOnTextChanged { text, _, _, _ ->
                viewModel.productSku(text.toString())
                checkPriceSkuField(text.toString())
            }
        }
    }

    private fun checkLengthName(length: Int) {
        if (length <= 10) {
            binding.tilProductName.error = "El nombre debe estar entre 10 y 150 caracteres"
        } else {
            binding.tilProductName.error = null
        }
    }

    private fun checkPriceEmptyField(field: String) {
        if (field.isEmpty()) {
            binding.tilProductPrice.error = "No debe de estar vacio"
        } else {
            binding.tilProductPrice.error = null
        }
    }

    private fun checkPriceSkuField(field: String) {
        if (field.isEmpty()) {
            binding.tilProductSku.error = "No debe de estar vacio"
        } else {
            binding.tilProductSku.error = null
        }
    }

    private fun addNewMarker(location: LatLng) {
        viewModel.addLocation(location)
        googleMap?.let {
            it.addMarker(
                MarkerOptions()
                    .position(location)
            )
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15f))
            it.setOnMyLocationButtonClickListener {
                setupFusedLocation()
                false
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        googleMap?.let {
            it.uiSettings.apply {
                setAllGesturesEnabled(false)
                isZoomControlsEnabled = false
            }
        }
        checkLocationMap()
    }

    private fun checkLocationMap() {
        if (isByEdit.not()) {
            enableMyLocation()
        } else {
            productToEdit?.let {
                addNewMarker(it.location)
            }
        }
    }

    private fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            googleMap?.isMyLocationEnabled = true
            setupFusedLocation()
            return
        }

        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            LOCATION_PERMISSION_REQUEST_CODE
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            super.onRequestPermissionsResult(
                requestCode,
                permissions,
                grantResults
            )
            return
        }

        if (isPermissionGranted(
                permissions,
                grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) || isPermissionGranted(
                permissions,
                grantResults,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        ) {
            enableMyLocation()
        }
    }

    companion object {
        const val IS_BY_EDIT = "is_by_edit"
        const val PRODUCT_TO_EDIT = "product_to_edit"
        const val LOCATION_PERMISSION_REQUEST_CODE = 1293
    }


}