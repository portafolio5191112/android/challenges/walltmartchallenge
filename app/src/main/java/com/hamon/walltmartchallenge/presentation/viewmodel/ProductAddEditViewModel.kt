package com.hamon.walltmartchallenge.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.hamon.walltmartchallenge.core.utils.BaseViewModel
import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.core.utils.UiState
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.domain.usecase.AddProductUseCase
import com.hamon.walltmartchallenge.domain.usecase.CreateUUIDUseCase
import com.hamon.walltmartchallenge.domain.usecase.UpdateProductUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductAddEditViewModel(
    private val createUUIDUseCase: CreateUUIDUseCase,
    private val addProductUseCase: AddProductUseCase,
    private val updateProductUseCase: UpdateProductUseCase
) : BaseViewModel<UiState<Any>>() {

    private val _createUUID: MutableLiveData<String> = MutableLiveData()
    val createUUID: LiveData<String> get() = _createUUID

    private val _enableButtonContinue: MutableLiveData<Boolean> =
        MutableLiveData<Boolean>().apply { value = false }
    val enableButtonContinue: LiveData<Boolean> get() = _enableButtonContinue

    private var productName: String = ""
    private var productPrice: String = ""
    private var productUUID: String = ""
    private var location: LatLng = LatLng(0.0, 0.0)
    private var editProduct: ProductDomain? = null

    fun addProduct(
        name: String,
        description: String,
        price: String,
        quantity: String,
        sku: String
    ) {
        showLoading()
        isShowLoading = true
        viewModelScope.launch(Dispatchers.IO) {
            val result = addProductUseCase.invoke(
                params = ProductDomain(
                    name = name,
                    description = description,
                    price = price.toDouble(),
                    id = sku,
                    photos = mutableListOf("dajsdasd", "asdjasdahs", "asdhjagsdasd"),
                    quantity = quantity.toInt(),
                    location = location
                )
            )
            handleResult(result)
        }
    }

    fun editProduct(
        name: String,
        description: String,
        price: String,
        quantity: String
    ) {
        showLoading()
        isShowLoading = true
        viewModelScope.launch(Dispatchers.IO) {
            val result = updateProductUseCase.invoke(
                params = ProductDomain(
                    name = name,
                    description = description,
                    price = price.toDouble(),
                    id = editProduct?.id,
                    photos = mutableListOf("dajsdasd", "asdjasdahs", "asdhjagsdasd"),
                    quantity = quantity.toInt(),
                    location = editProduct?.location ?: location
                )
            )
            handleResult(result)
        }
    }

    private fun handleResult(state: TransactionState<Any>) {
        when (state) {
            is TransactionState.Success -> _uiState.postValue(UiState.Success())
            is TransactionState.Error -> _uiState.postValue(UiState.Error())
        }
    }

    fun createSku() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = createUUIDUseCase()
            _createUUID.postValue(result)
        }
    }

    private fun showLoading() {
        if (isShowLoading.not()) {
            _uiState.value = UiState.Loading
        }
    }

    fun productName(name: String) {
        productName = name
        checkButtonEnable()
    }

    fun productPrice(price: String) {
        productPrice = price
        checkButtonEnable()
    }

    fun productSku(sku: String) {
        productUUID = sku
        checkButtonEnable()
    }

    private fun checkButtonEnable() {
        _enableButtonContinue.value =
            productName.isNotEmpty() && productName.length in 10..150 && productPrice.isNotEmpty() && productUUID.isNotEmpty()
    }

    fun addLocation(location: LatLng) {
        this.location = location
    }

    fun editProductModel(product: ProductDomain?) {
        product?.let {
            editProduct = product
        }
    }
}