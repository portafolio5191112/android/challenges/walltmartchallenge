package com.hamon.walltmartchallenge

import android.app.Application
import com.google.android.gms.maps.GoogleMap
import com.google.android.libraries.places.api.Places
import com.hamon.walltmartchallenge.core.di.firebaseModule
import com.hamon.walltmartchallenge.core.di.remoteDatasource
import com.hamon.walltmartchallenge.core.di.repository
import com.hamon.walltmartchallenge.core.di.useCase
import com.hamon.walltmartchallenge.core.di.viewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Places.initialize(applicationContext,"AIzaSyCYCdQFc6Bfwu-N2JnQg6lIbEfBJfe1bUU" )
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                mutableListOf(
                    firebaseModule,
                    remoteDatasource,
                    repository,
                    useCase,
                    viewModel
                )
            )
        }
    }
}