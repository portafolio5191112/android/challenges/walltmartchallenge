package com.hamon.walltmartchallenge.domain.entities

import com.google.android.gms.maps.model.LatLng
import com.hamon.walltmartchallenge.data.model.LocationData

data class ProductDomain(
    val name: String,
    val description: String,
    val price: Double,
    val photos: MutableList<String>,
    val id: String? = null,
    val quantity: Int,
    val location: LatLng
)