package com.hamon.walltmartchallenge.domain.usecase

import com.hamon.walltmartchallenge.core.utils.UseCase
import com.hamon.walltmartchallenge.domain.repository.ProductsRepository

class CreateUUIDUseCase(private val repository: ProductsRepository): UseCase<Any, String>() {
    override suspend fun invoke(params: Any?): String {
        return repository.createUUID()
    }

}