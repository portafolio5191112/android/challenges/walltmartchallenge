package com.hamon.walltmartchallenge.domain.usecase

import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.core.utils.UseCase
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.domain.repository.ProductsRepository

class GetProductListUseCase(private val repository: ProductsRepository): UseCase<Any, TransactionState<MutableList<ProductDomain>>>() {
    override suspend fun invoke(params: Any?): TransactionState<MutableList<ProductDomain>> {
        return try {
            TransactionState.Success(value  = repository.getProductList())
        } catch (exception: Exception) {
            TransactionState.Error()
        }
    }

}