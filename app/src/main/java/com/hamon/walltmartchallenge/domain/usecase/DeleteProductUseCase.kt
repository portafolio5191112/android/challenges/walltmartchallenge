package com.hamon.walltmartchallenge.domain.usecase

import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.core.utils.UseCase
import com.hamon.walltmartchallenge.domain.repository.ProductsRepository

class DeleteProductUseCase(private val repository: ProductsRepository): UseCase<String, TransactionState<Any>>() {
    override suspend fun invoke(params: String?): TransactionState<Any> {
        return repository.deleteProduct(params)
    }
}