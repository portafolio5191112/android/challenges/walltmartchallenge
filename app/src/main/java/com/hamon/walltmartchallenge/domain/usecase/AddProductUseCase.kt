package com.hamon.walltmartchallenge.domain.usecase

import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.core.utils.UseCase
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.domain.repository.ProductsRepository

class AddProductUseCase(private val repository: ProductsRepository): UseCase<ProductDomain, TransactionState<Any>>() {

    override suspend fun invoke(params: ProductDomain?): TransactionState<Any> {
        try {
            params?.let {
                repository.addProduct(params)
                return TransactionState.Success()
            } ?: kotlin.run {
                return TransactionState.Error()
            }
        } catch (exception: Exception) {
            return TransactionState.Error()
        }
    }

}