package com.hamon.walltmartchallenge.domain.usecase

import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.core.utils.UseCase
import com.hamon.walltmartchallenge.domain.entities.ProductDomain
import com.hamon.walltmartchallenge.domain.repository.ProductsRepository

class UpdateProductUseCase(private val repository: ProductsRepository) :
    UseCase<ProductDomain, TransactionState<Any>>() {
    override suspend fun invoke(params: ProductDomain?): TransactionState<Any> {
        params?.let {
            repository.updateProduct(params)
            return TransactionState.Success()
        } ?: run {
            return TransactionState.Error()
        }
    }

}