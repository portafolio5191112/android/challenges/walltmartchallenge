package com.hamon.walltmartchallenge.domain.repository

import com.hamon.walltmartchallenge.core.utils.TransactionState
import com.hamon.walltmartchallenge.data.model.ProductData
import com.hamon.walltmartchallenge.domain.entities.ProductDomain

interface ProductsRepository {
    suspend fun getProductList(): MutableList<ProductDomain>

    suspend fun addProduct(productDomain: ProductDomain): TransactionState<Any>

    suspend fun updateProduct(productToUpdate: ProductDomain): TransactionState<Any>

    suspend fun deleteProduct(productId: String?): TransactionState<Any>

    fun createUUID(): String
}